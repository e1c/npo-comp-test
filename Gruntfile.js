module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
	  autoprefixer: {
		  options: {
			  browsers: ['> 1% in RU', 'last 2 versions', 'ie 9'],
			  cascade: false,
			  map: {
				  prev: 'css/',
				  inline: false
			  }
		  },
		  css: {
			  src: 'css/style.css',
			  dest: 'css/style.css'
		  }
	  },
    jshint: {
      options: {
        eqeqeq: true,
	      futurehostile: true,
        latedef: true,
        noarg: true,
        undef: true,
        unused: true,
        eqnull: true,
        browser: true,
	      jquery: true,
        globals: {
	        'module': true,
	        'ymaps': true,
	        'Cookies': true,
	        'fotoramaDefaults': true
        }
      },
	    gruntfile: {
		    src: 'Gruntfile.js'
	    },
	    js: {
		    src: 'js/*.js'
	    }
    },
	  less: {
		  development: {
			  options: {
				  paths: "less",
				  sourceMap: true,
				  sourceMapRootpath: '/'
			  },
			  files: {
				  'css/style.css': 'less/style.less'
			  }
		  }
	  },
    watch: {
	    options: {
		    interrupt: true
	    },
	    gruntfile: {
		    files: '<%= jshint.gruntfile.src %>',
		    tasks: ['jshint:gruntfile']
	    },
	    js: {
		    files: '<%= jshint.js.src %>',
		    tasks: ['jshint:js']
	    }
    }
  });

	grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('css', ['less', 'autoprefixer']);
	grunt.registerTask('dev', ['watch']);

};
