<?php
$error_line = 'ERROR at '.date("Y-m-d H:i:s", $_SERVER['REQUEST_TIME'])."\n";
foreach($_POST['data'] as $key => $value) {
    $error_line .= $key.': '.$value."\n";
}
$error_line .= 'user agent = '.$_SERVER['HTTP_USER_AGENT']."\n\n";
file_put_contents('logs/frontend.log', $error_line, FILE_APPEND | LOCK_EX);