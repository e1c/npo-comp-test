var default_floor = 0,
		write_config = true; // error logging on/off

// Dealing with errors
// Saving errors to log file
var errorLog = function(e) {
	if (write_config) $.post('/save_log.php', {data:e});
};

// Overall error handler
window.onerror = function(message, url, lineNumber, errorObj) {
	errorLog({
		title     : 'Error in js code',
		message   : message,
		url       : url,
		lineNumber: lineNumber,
		StackTrace: errorObj
	});
};

// Retrieving floor plan (svg image and json file)
var loadFloorPlan = function() {
	var svg = '/img/floor' + $(this).data('floor') + '.svg',
			menu_item  = $(this).prop("tagName").toLowerCase() + '.' + $(this).attr('class') + ':eq(' + $(this).index() + ')';
	$('.menu__item').removeClass('selected');
	$(this).addClass('selected');
	$('#floorplan').empty().load(svg, function (data, textStatus, jqXHR) {
		if (textStatus === "error") {
			errorLog({
				title  : 'floor image file was not loaded',
				file   : svg,
				'http status'   : jqXHR.status,
				'clicked target': menu_item,
				'js function'   : 'loadFloorPlan'
			});
		}
	});
	$('#floorlist').empty();
	var json = '/data/floor' + $(this).data('floor') + '.json';
	$.getJSON(json, function(data) {
		var items = [];
		$.each(data.rooms, function(key, val) {
			items.push( "<li class='floorlist__item js_room_list' id='" + key + "'>" + val + "</li>" );
		});
		$('#floorlist').append(items.join(''));
	}).fail(function(data, textStatus, jqXHR) {
		errorLog({
			title  : 'json file error',
			file   : json,
			'http status'   : jqXHR.status,
			'clicked target': menu_item,
			'js function'   : 'loadFloorPlan'
		});
	});
};

// binding event to change floors and displaying default floor plan
var initLoadFloorPlan = function() {
	$(document).on('click', '.menu__item:not(.selected)', loadFloorPlan);
	$('.menu__item:eq(' + default_floor + ')').click();
};

// Highlighting room's name when room is hovered on the plan
var highlightRoomList = function() {
	var id = $(this).attr('id').substring(4);
	$('#r' + id).addClass('selected');
};
var unhighlightRoomList = function() {
	var id = $(this).attr('id').substring(4);
	$('#r' + id).removeClass('selected');
};

// Highlighting room's area on the plan when room is hovered in the list
var highlightRoomPlan = function() {
	var room = '#room' + $(this).attr('id').substring(1),
		  cls = $(room).attr('class');
	$(room).attr('class', cls + ' selected');
};
var unhighlightRoomPlan = function() {
	var room = '#room' + $(this).attr('id').substring(1),
		cls = $(room).attr('class');
	$(room).attr('class', cls.replace('selected',''));
};

// Binding hover events to floor plan and room list items
var initRoomHighlight = function() {
	$(document).on('mouseover', '.room', highlightRoomList);
	$(document).on('mouseleave', '.room', unhighlightRoomList);
	$(document).on('mouseover', '.js_room_list', highlightRoomPlan);
	$(document).on('mouseleave', '.js_room_list', unhighlightRoomPlan);
};

$(function() {
	initLoadFloorPlan();
	initRoomHighlight();
});